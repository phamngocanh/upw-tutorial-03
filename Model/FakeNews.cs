﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;


namespace FakeNews.Model
{
  public  class NewsItem
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string Headline { get; set; }
        public string DateLine { get; set; }
        public string Subhead { get; set; }
        public string Image { get; set; }

    }
    public class NewsManager
    {
        public static void GetNews(string category,ObservableCollection<NewsItem> newsItems)
        {
            var allItems = getNewsItems();
            var filteredNewsItems=allItems.Where(p => p.Category==category).ToList();
            newsItems.Clear();
            filteredNewsItems.ForEach(p => newsItems.Add(p));
        }
        private static List<NewsItem> getNewsItems()
        {
            var items = new List<NewsItem>();
            items.Add(new NewsItem() { Id = 1, Category = "Financial",Headline="Lorem Ipsum" ,DateLine="Nunc Tristique nec", Subhead="Doro sit amet", Image="Assets/upw3/Financial1.png " });
            items.Add(new NewsItem() { Id = 2, Category = "Financial", Headline = "Etian ac felis viverra", DateLine = "tortor porttitor,eu fermentum ante congue", Subhead = "vulputata nisl ac , aliquet nisi" ,Image = "Assets/upw3/Financial2.png " });
            items.Add(new NewsItem() { Id = 3, Category = "Financial", Headline = "Integer sed turpis erat", DateLine = "in viverra metus facilisis sed", Subhead = "Sed quis hendrerit lorem , quis interdum dolor", Image = "Assets/upw3/Financial3.png " });
            items.Add(new NewsItem() { Id = 4, Category = "Financial", Headline = "Proin sem neque", DateLine = "Integer eleifend", Subhead = "aliquet quis ipsum tincidunt" ,Image = "Assets/upw3/Financial4.png " });
            items.Add(new NewsItem() { Id = 5, Category = "Financial", Headline = "Mauris bibendum non leo vitae tempor", DateLine = "Curabitur dictum augue vitae element ultrices" ,Subhead = "In nisl tortor , eleifend sed ipsum eget" ,Image = "Assets/upw3/Financial5.png " });
            items.Add(new NewsItem() { Id = 6, Category = "Food", Headline = "Lorem ispum", DateLine = "Nunc tristique" ,Subhead = "Dolor sit amet", Image = "Assets/upw3/Food1.png " });
            items.Add(new NewsItem() { Id = 7, Category = "Food", Headline = "Etiam ac felis viverra", DateLine = "Nunc tristique" ,Subhead = "Dolor sit amet", Image = "Assets/upw3/Food2.png " });
            items.Add(new NewsItem() { Id = 8, Category = "Food", Headline = "Integer sed turpis erat", DateLine = "Nunc tristique" ,Subhead = "Dolor sit amet" ,Image = "Assets/upw3/Food3.png " });
            items.Add(new NewsItem() { Id = 9, Category = "Food", Headline = "proin sem neque", DateLine = "Nunc tristique", Subhead = "Dolor sit amet", Image = "Assets/upw3/Food4.png " });
            items.Add(new NewsItem() { Id = 10, Category = "Food", Headline = "Mauris bibendum non leo vitae tempor", DateLine = "" ,Subhead = "Dolor sit amet", Image = "Assets/upw3/Food5.png " });
            return items;
        }
    }
}
